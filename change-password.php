<html>  
<head lang="en">  
    <meta charset="UTF-8">  
    <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">  
    <title>Forgot Password</title>  
</head>  
<style>  
    .login-panel {  
        margin-top: 150px;  
  
</style>  
<body>  
  
<div class="container"><!-- container class is used to centered  the body of the browser with some decent width-->  
    <div class="row"><!-- row class is used for grid system in Bootstrap-->  
        <div class="col-md-4 col-md-offset-4"><!--col-md-4 is used to create the no of colums in the grid also use for medimum and large devices-->  
            <div class="login-panel panel panel-success">  
                <div class="panel-heading">  
                    <h3 class="panel-title">Forgot Password</h3>  
                </div>  
                <div class="panel-body">  
                    <form role="form" method="post" action="change-password.php">  
                        <fieldset>  
							<div class="form-group">  
                                <input class="form-control" placeholder="Username" name="name" type="text" autofocus>  
                            </div>  
                            <div class="form-group">  
                                <input class="form-control" placeholder="Old Password" name="oldpassword" type="password" autofocus>  
                            </div>  
  
                            <div class="form-group">  
                                <input class="form-control" placeholder="New Password" name="newpassword" type="password" autofocus>  
                            </div>   
							 <div class="form-group">  
                                <input class="form-control" placeholder="Re-enter Password" name="newpasswordagain" type="password" autofocus>  
                            </div> 
  
  
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="Change Password" name="changepass" >  
							
  
                        </fieldset>  
                    </form>  	
                </div>  
            </div>  
        </div>  
    </div>  
</div>  
  
</body>  
  
</html>  

<?php
include("db_connection.php");
if(isset($_POST['changepass']))
{
	$user_name=$_POST['name'];
	$oldpassword=$_POST['oldpassword'];
	$newpassword=$_POST['newpassword'];
	$newpasswordagain=$_POST['newpasswordagain'];
	if($newpasswordagain == $newpassword){
		$check_email_query="select * from users WHERE user_name='$user_name'"; 
		$run_query=$conn->query($check_email_query);
		if($run_query->fetch_assoc()<0){
			echo "<script>alert('Email $user_email does not exists, Please try another one!')</script>";  
			exit(); 
		}
		$update_password = "UPDATE users set user_pass='$newpassword' where user_name='$user_name' AND user_pass='$oldpassword'";
		echo $update_password;
		if($conn->query($update_password))  
		{  
			echo "<script>alert('Password updated successfully')</script>";
			echo"<script>window.open('login.php','_self')</script>";  
		}  
	}
	else{
		echo"<script>alert('Password does not match')</script>";  
	}
}
	

?> 